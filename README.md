# B.I.L.L #

B.I.L.L est un projet Unity développé afin de mettre en pratique ce que j'ai appris après  
avoir suivi l'excellent tutoriel d'introduction à Unity sur le site Pluralsight.

Ce tutoriel propose de créer un petit-jeu en vue à la troisième personne permettant de contrôler  
un petit robot devant récupérer des boules d'énergie, tout en évitant les ennemis présents sur son chemin.

J'avais décidé de suivre cette formation care j'étais intrigué par la manière dont 
sont développés les jeux-vidéos de nos jours.  
J'en ai donc appris beaucoup dans ce domaine tout au long de ce projet.


![bill.PNG](https://bitbucket.org/repo/7EEaggK/images/1926531100-bill.PNG)



## Configuration requise ##
* Le logiciel Unity
* Visual Studio C#

## Comment jouer à *B.I.L.L* ? ##

Pour lancer le logiciel, il est nécessaire de charger le projet avec Unity et de l'exécuter.  
Unity va alors créer un fichier exécutable permettant de lancer directement le jeu.

## Raccourcis clavier ##


* Pour se déplacer, utilisez les touches directionnelles.